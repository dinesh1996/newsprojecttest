let express = require('express');
let router = express.Router();
let controller = require('../controllers/news-controller');
let auth = require('../utils/validate-token');


/**
* @typedef Comment
* @property {string} content.required
*/


/**
 * Search news
 * @route GET /api/news/search
 * @group news - Operations about news
 * @param {string} name.query - name of news
 * @returns {Array.<News>} 200 - An array of news
 * @returns {Error} - Unexpected error
 */
router.route('/search')
    .get(auth.token, controller.searchNews)

/**
 * @route GET /api/news
 * @group news - Operations about news
 * @returns {Array.<News>} 200 - An array of news
 * @returns {Error} - Unexpected error
 */
/**
 * @route POST /api/news
 * @group news - Operations about news
 * @param {News} news - news to insert
 * @returns {News} 200 - News created
 * @returns {Error} - Unexpected error
 */
router.route('/')
    .get(controller.getAllNews)
    .post(auth.token, auth.admin, controller.createNews);

/**
 * @route GET /api/news/:id
 * @group news - Operations about news
 * @param {string} id - id to find news
 * @returns {News} 200 - An news
 * @returns {Error} - Unexpected error
 */
/**
* @route PUT /api/news/:id
* @group news - Operations about news
* @param {string} id.query - id to update news
* @param {News} news - News to update
* @returns {News} 200 - News updated
* @returns {Error} - Unexpected error
*/
/**
 * @route DELETE /api/news/:id
 * @group news - Operations about news
 * @param {string} id - id to remove News
 * @returns {News} 200 - News deleted
 * @returns {Error} - Unexpected error
 */
router.route('/:id')
    .get(controller.getNewstById)
    .put(auth.token, auth.admin, controller.updateNews)
    .delete(auth.token, auth.admin, controller.deleteNews);


/**
 * @route POST /api/news/comment/:id
 * @group comments - Operations about comment
 * @param {News} news - comment to insert in a news
 * @returns {News} 200 - News created
 * @returns {Error} - Unexpected error
 */
router.route('/comment/:id')
    .post(auth.token, controller.addCommentNews)

module.exports = router;