let express = require('express');
let router = express.Router();
let controller = require('../controllers/users-controller');
let auth = require('../utils/validate-token');

/**
 * @typedef User
 * @property {string} name.required
 * @property {string} firstname.required
 * @property {string} password.required
 * @property {bool} admin.required
 * @property {string} status
 */


 /**
 * @route GET /api/users
 * @group users - Operations about users
 * @returns {Array.<User>} 200 - An array of users
 * @returns {Error} - Unexpected error
 */
/**
 * @route POST /api/users
 * @group users - Operations about user
 * @param {User} user - User to insert
 * @returns {User} 200 - User created
 * @returns {Error} - Unexpected error
 */
//router.get('/', controller.getAllUsers);
router.route('/')
    .get(auth.token, controller.getAllUsers)
    .post(controller.createUser);


 /**
 * @route GET /api/users/:id
 * @group users - Operations about user
 * @returns {User} 200 -  user
 * @returns {Error} - Unexpected error
 */

  /**
 * @route PUT /api/users/:id
 * @group users - Operations about user
 * @param {User} user - Update user info 
 * @returns {User} 200 - User created
 * @returns {Error} - Unexpected error
 */

/**
 * @route POST /api/users/:id
 * @group users - Operations about user
 * @param {User} user - User to insert
 * @returns {User} 200 - User  status and admin changed
 * @returns {Error} - Unexpected error
 */


router.route('/:id')
    .get(auth.token, controller.getUserById)
    .put(auth.token, controller.updateUser)
    .post(auth.admin, controller.beAdmin)

module.exports = router;