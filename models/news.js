let mongoose = require('mongoose');

let Schema = mongoose.Schema;

module.exports = mongoose.model('News', new Schema({
    name: {
        type: String,
        required: 'Nom obligatoire'
    },
    comments: [{ type: Schema.Types.ObjectId, ref: 'Comment' }],
    content: {
        type: String,
        required: 'Contenu obligatoire'
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },

    createdBy: {
        type: Schema.Types.ObjectId, ref: 'User'
    }
}));