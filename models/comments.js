let mongoose = require('mongoose');

let Schema = mongoose.Schema;

module.exports = mongoose.model('Comment', new Schema({
    content: String,
    createdAt: {
        type: Date,
        default: Date.now()
    }
}));