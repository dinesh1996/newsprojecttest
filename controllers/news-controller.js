let comments = require('../models/comments');
let news = require('../models/news');


// GET /
exports.getAllNews = (req, resp) => {
    news.find({}).sort({ 'createdAt': -1 }).populate({ path: 'comments', options: { sort: { 'createdAt': -1 } } }).populate('createdBy').
        exec((err, data) => {
            resp.json(extractData(err, data));

        });
};


// GET /:id
exports.getNewstById = (req, resp) => {
    news.find({}).populate({ path: 'comments', options: { sort: { 'createdAt': -1 } } }).populate('createdBy').
        exec((err, data) =>
            resp.json(extractData(err, data)
            ));
};


// GET /search?....
exports.searchNews = (req, resp) => {
    const params = req.query;
    let pred = {};
    if (params.name)
        pred.name = { $regex: params.name, $options: 'i' };

    news.find(pred).populate('createdBy').populate('comments').
        exec((err, data) =>
            resp.json(extractData(err, data)
            ));
}

//POST /
exports.createNews = (req, resp) => {
    let obj = new news();
    obj.name = req.body.name;
    obj.content = req.body.content;
    obj.createdBy = req.app.get('user').id;
    obj.save((err, data) => resp.json(extractData(err, data)));
}

// PUT /:id
exports.updateNews = (req, resp) => {
    news.findByIdAndUpdate(req.params.id,
        {
            name: req.body.name,
            content: req.body.content,
        },
        { new: false }, (err, data) => resp.json(extractData(err, data)));
}

exports.updateNews = (req, resp) => {
    news.findByIdAndUpdate(req.params.id,
        {
            name: req.body.name,
            content: req.body.content,
        },
        { new: false }, (err, data) => resp.json(extractData(err, data)));
}

exports.addCommentNews = (req, resp) => {
    newComment = new comments();
    newComment.content = req.body.content;
    console.log(newComment);
    newComment.save();
    news.findByIdAndUpdate(req.params.id,
        {
            $push: { comments: newComment }
        },
        { new: false }, (err, data) => resp.json(extractData(err, data)));
}


//DELETE /:id
exports.deleteNews = (req, resp) => {
    news.remove({ _id: req.params.id }, (err, data) => {
        if (err)
            resp.json(err);
        resp.json({ 'message': 'News successfully deleted' });
    })
}

function extractData(err, data) {
    if (err)
        return err;
    return data;
}
