let users = require('../models/users');
let sha1 = require('../utils/sha1');

// GET /
exports.getAllUsers = (req, resp) => {
    users.find({}, (err, data) => {
        resp.json(extractData(err, data));
    });
}

// GET /:id
exports.getUserById = (req, resp) => {
    users.findById(req.params.id, (err, data) => {
        resp.json(extractData(err, data));
    });
}


//POST /
exports.createUser = (req, resp) => {
    let obj = new users(req.body);
    let regexp = /^.*(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^\da-zA-Z]){6,}.*$/;
    obj.status = 'pending';
    obj.admin = false;
    if (regexp.test(obj.password)) {
        obj.password = sha1(obj.password);
        obj.save((err, data) => resp.json(extractData(err, data)));
    } else {
        resp.status(400).json({ success: false, message: 'password failed.' });
    }
}

// PUT /:id
exports.updateUser = (req, resp) => {
    let regexp = /^.*(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^\da-zA-Z]){6,}.*$/;
    let user = req.app.get('user');

    if (user.admin || user.id === req.params.id) {
        if (regexp.test(req.body.password)) {
            users.findByIdAndUpdate(req.params.id, {
                name: req.body.name,
                firstname: req.body.name,
                password: sha1(req.body.password)
            }, { new: false }, (err, data) => resp.json(extractData(err, data)));
        }
    } else {
        resp.json({ 'message': 'User not updated' });
    }
}

//DELETE /:id
exports.deleteUser = (req, resp) => {
    users.remove({ _id: req.params.id }, (err, data) => {
        if (err)
            resp.json(err);
        resp.json({ 'message': 'User successfully deleted' });
    })
}

// BeAdmin /:id
exports.beAdmin = (req, resp) => {
    users.findByIdAndUpdate(req.params.id, {
        admin: req.body.admin,
        status: req.body.status,
    }, { new: false }, (err, data) => resp.json(extractData(err, data)));

}

function extractData(err, data) {
    if (err)
        return err;
    return data;
}
