let express = require('express');
let mongoose = require('mongoose');
let bodyParser = require('body-parser');

let usersRoutes = require('./routes/users-routes');
let newsRoutes = require('./routes/news-routes');
let authController = require('./controllers/authentication-controller');

let auth = require('./utils/validate-token');
let cors = require('cors');
let app = express();


app.use(cors());
let swag = require('express-swagger-generator')(app);
let options = require('./swagger');
swag(options);

const confMongo = require('./configurations/config-mongo');
// mongoose.Promise = global.Promise; V4
mongoose.connect(confMongo.database);

app.use(bodyParser.urlencoded({ extended: false })); //URL
app.use(bodyParser.json()); //BODY

app.post('/authentication', authController);

app.use('/api/users', usersRoutes);
app.use('/api/news', newsRoutes);

app.route('/').get((req, resp) => {
    resp.json('WEB API');
});

app.listen(8888);

module.exports = app;

