module.exports = {
    swaggerDefinition: {
        info: {
            description: '',
            title: 'WEB API en NodeJS',
            version: '1.0.0',
        },
        host: 'localhost: 8888',
        basePath: '/api',
        produces: [
            "application/json",
            "application/xml"
        ],
        schemes: ['http', 'https'
        ],
        securityDefinitions: {
            JWT: {
                type: 'apiKey',
                in: 'header',
                name: 'Authorization',
                description: "You need a APIKey to get acceses",
            }
        }
    },
    basedir: __dirname, //app absolute path
    files: ['./routes/*.js'] //Path to the API handle folder
};